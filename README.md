## Sample Blog App ##

This sample application uses... 

* Angular
* Textbox.io Directive (https://github.com/ephox/angular-textbox.io)
* Textbox.io Factories *[optional]* (https://github.com/ephox/angular-textbox.io)
module.exports = function (express, app) {
	var router = express.Router();
	var Post = require('../models/post.js');
	var Category = require('../models/category.js');
	var util = require('util');

	var pageSize = 5;

	// Middleware to log requests...
	router.use(function (req, res, next) {
		console.log(util.inspect(req.originalUrl, {
			showHidden: false,
			depth: 1
		}));
		next(); //if you don't do this the request stops here...
	});

	// test route - to make sure things work at first
	router.get('/', function (req, res) {
		res.json({
			message: 'Holy cow!  This actually worked!  See our documentation for the APIs you should **really** be calling.'
		});
	});

	// REST API routes for POST
	router.route('/posts/search')
		// fetch a total count of all posts
		.get(function (req, res) {
			Post.find({
					$text: {
						$search: req.query.search_string
					}
				})
				.sort({
					lastUpdated: -1
				})
				.exec(function (err, posts) {
					if (err) {
						console.log('ERROR ' + err);
						res.send(err);
					} else {
						res.json(posts);
					}
				});
		});

	router.route('/posts/total')
		// fetch a total count of all posts
		.get(function (req, res) {
			var findParams = {};
			if (req.query.category) {
				findParams.category = req.query.category;
				//console.log('About to ask for data with category: ' + JSON.stringify(findParams));
			}
			Post.count(findParams, function (err, count) {
				if (err) {
					console.log('ERROR ' + err);
					res.send(err);
				} else {
					//console.log("Count:", JSON.stringify(count));
					res.json(count);
				}
			});
		});

	router.route('/posts/:post_id')
		// fetch a single post
		.get(function (req, res) {
			Post.findById(req.params.post_id).populate('category').exec(function (err, post) {
				if (err) {
					res.send(err);
				} else {
					//post.tags = post.tags.replace(/,/g, " ");
					res.json(post);
				}
			});
		})
		// update a single post
		.put(function (req, res) {
			//console.log('POSTING...');
			Post.findById(req.params.post_id, function (err, post) {
				if (err) {
					console.log('ERROR ' + err);
					res.send(err);
				} else {
					//TODO: Update ALL fields that are included in the form data
					post.title = req.body.title;
					post.author = req.body.author;
					var tags = "";
					if (req.body.tags) {
						tags = req.body.tags.match(/\w+|"(?:\\"|[^"])+"/g);
						//console.log('Tags: ' + tags);
					}
					post.tags = tags;
					if (req.body.category) {
						//console.log('Adding category:' + req.body.category._id);
						post.category = req.body.category._id;
					}
					post.imageURL = req.body.imageURL;
					post.teaserContent = req.body.teaserContent;
					post.fullContent = req.body.fullContent;
					// Not updating the creation date!
					// Adding a lastUpdated property
					post.lastUpdated = new Date();
					post.save(function (err) {
						if (err) {
							res.send(err);
						} else {
							res.json({
								message: 'POST Updated with id: ' + post._id
							});
						}
					});
				}
			});
		})
		// delete a single post
		.delete(function (req, res) {
			Post.remove({
				_id: req.params.post_id
			}, function (err, post) {
				if (err) {
					res.send(err);
				} else {
					res.json({
						message: 'POST Deleted with id: ' + req.params.post_id
					});
				}
			});
		});

	router.route('/posts')
		// create a post
		.post(function (req, res) {
			console.log('Starting process to save a new post... ');
			console.log('Title: ' + req.body.title);
			console.log('Author: ' + req.body.author);
			var post = new Post(); //use our mongoose Model
			post.title = req.body.title
			post.author = req.body.author;
			var tags = "";
			if (req.body.tags) {
				tags = req.body.tags.match(/\w+|"(?:\\"|[^"])+"/g);
				//console.log('Tags: ' + tags);
			}
			post.tags = tags;
			if (req.body.category) {
				//console.log('Adding category:' + req.body.category._id);
				post.category = req.body.category._id;
			}
			post.postDate = req.body.postDate;
			post.lastUpdated = req.body.postDate;
			post.imageURL = req.body.imageURL;
			post.teaserContent = req.body.teaserContent;
			post.fullContent = req.body.fullContent;
			console.log('About to save...');
			post.save(function (err) {
				if (err) {
					console.log('ERROR Saving!');
					res.send(err);
				} else {
					console.log('Save worked!');
					res.json({
						message: 'NEW POST Created with id: ' + post._id
					});
				}
			});
		})
		// fetch all posts
		.get(function (req, res) {
			//Don't include fullContent field as we don't show it on the home page
			var findParams = {};
			if (req.query.category) {
				findParams.category = req.query.category;
			}
			var currentPage = 1;
			if (req.query.page) {
				currentPage = req.query.page;
			}

			Post.find(findParams)
				.sort({
					lastUpdated: -1
				})
				.skip(pageSize * (currentPage - 1))
				.limit(pageSize)
				.select('-fullContent').exec(function (err, posts) {
					if (err) {
						res.send(err);
					} else {
						res.json(posts);
					}
				});
		});

	// END REST API routes for POST

	// REST API routes for CATEGORY
	router.route('/categories/:category_id')
		// fetch a single post
		.get(function (req, res) {
			Category.findById(req.params.category_id, function (err, category) {
				if (err) {
					res.send(err);
				} else {
					res.json(category);
				}
			});
		});
	router.route('/categories')
		// fetch all categories
		.get(function (req, res) {
			Category.find({}, null, {
				sort: {
					name: 1
				}
			}, function (err, categories) {
				if (err) {
					res.send(err);
				} else {
					res.json(categories);
				}
			});
		});
	// END REST API routes for CATEGORY

	// REGISTER OUR ROUTES
	// =================================================
	app.use('/api', router);

	// frontend routes =========================================================
	// route to handle all angular requests
	app.use('*', function (req, res) {
		res.redirect('/');
	});
}

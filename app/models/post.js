var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = new Schema({
	title: String,
	author: String,
	tags: String,
	postDate: Date,
	lastUpdated: Date,
	imageURL: String,
	teaserContent: String,
	fullContent: String,
	category: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'}
});

module.exports = mongoose.model('Post', PostSchema);

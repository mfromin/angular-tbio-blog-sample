angular.module('blogApp').factory('postFactory', ['$http', '$log', function ($http, $log) {

	var urlBase = '/api/posts';
	var dataFactory = {};

	dataFactory.getPosts = function (currentPage, currentCategory) {
		var search_params = {};
		search_params.page = currentPage;
		search_params.category = currentCategory;
		//console.log('SearchParams:' + JSON.stringify(search_params));
		return $http.get(urlBase, {
			params: search_params
		});
	};

	dataFactory.getPostsSearch = function (searchString) {
		var search_params = {};
		search_params.search_string = searchString;
		//$log.log('SearchParams:' + JSON.stringify(search_params));
		return $http.get(urlBase + '/search', {
			params: search_params
		});
	};

	dataFactory.getPost = function (id) {
		return $http.get(urlBase + '/' + id);
	};

	dataFactory.insertPost = function (post) {
		return $http.post(urlBase, post);
	};

	dataFactory.updatePost = function (post) {
		return $http.put(urlBase + '/' + post._id, post)
	};

	dataFactory.deletePost = function (id) {
		return $http.delete(urlBase + '/' + id);
	};

	dataFactory.getTotalPosts = function (currentCategory) {
		var search_params = {};
		search_params.category = currentCategory;
		//console.log('Current category for API call: ' + JSON.stringify(search_params));
		return $http.get(urlBase + '/total', {
			params: search_params
		});
	};

	return dataFactory;
}]);

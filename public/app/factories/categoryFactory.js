angular.module('blogApp').factory('categoryFactory', ['$http', function ($http) {

	var urlBase = '/api/categories';
	var dataFactory = {};

	dataFactory.getCategories = function () {
		return $http.get(urlBase);
	};

	dataFactory.getCategory = function (id) {
		return $http.get(urlBase + '/' + id);
	};

//	dataFactory.insertPost = function (post) {
//		return $http.post(urlBase, post);
//	};
//
//	dataFactory.updatePost = function (post) {
//		return $http.put(urlBase + '/' + post._id, post)
//	};
//
//	dataFactory.deletePost = function (id) {
//		return $http.delete(urlBase + '/' + id);
//	};

	return dataFactory;
}]);

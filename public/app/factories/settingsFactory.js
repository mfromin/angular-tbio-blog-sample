angular.module('blogApp').factory('settingsFactory', function () {

	var current_category_id = "";
	var current_page = 1;
	var page_size = 5;
	var settings = {};

	//current_category_id
	settings.getCategoryId = function () {
		return current_category_id;
	}

	settings.setCategoryId = function (newCategoryId) {
		current_category_id = newCategoryId;
	}

	//current_page
	settings.getCurrentPage = function () {
		return current_page;
	}

	settings.setCurrentPage = function (newPage) {
		current_page = newPage;
	}

	//page_size
	settings.getPageSize = function () {
		return page_size;
	}

	//settings.setPageSize = function (newPageSize) {
	//	page_size = newPageSize;
	//}

	return settings;
});

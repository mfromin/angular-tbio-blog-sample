angular.module('blogApp').controller('BlogPostsController', ['$scope', '$routeParams', '$log', '$location', '$anchorScroll', 'postFactory', 'settingsFactory', function ($scope, $routeParams, $log, $location, $anchorScroll, postFactory, settingsFactory) {
	//$log.log('Loading BlogPostsController');
	var pageToFetch = 1;
	var categoryToFetch = null;
	if ($location.search().page) {
		//$log.log('Page requested: ' + JSON.stringify($location.search().page));
		var tempPage = parseInt($location.search().page);
		if (typeof tempPage === 'number' && (tempPage % 1) === 0) {
			pageToFetch = tempPage;
		} else {
			$location.search('page', null);
		}
	}

	if ($location.search().category) {
		//$log.log('Category requested: ' + JSON.stringify($location.search().category));
		categoryToFetch = $location.search().category;
	} else {
		$location.search('category', null);
	}

	$scope.status; //currently not used - could use to state status on page.
	$scope.posts;
	$scope.pageTitles = {
		pageHeading: "Fore Left!",
		secondaryText: "  One Man's Search For Golf Nirvana"
	};
	$scope.pagingFlags = {
		isNewer: false,
		isOlder: true,
		totalPages: 1,
		currentPage: 1,
		postCount: 0
	};
	getPosts();

	$scope.changePage = function (pageIncrement) {
		pageToFetch = pageToFetch + pageIncrement;
		settingsFactory.setCurrentPage(pageToFetch);
		getPosts();
		$location.search('page', pageToFetch);
		$anchorScroll('top');
	};

	function getPosts() {
		postFactory.getTotalPosts(categoryToFetch)
			.success(function (count) {
				//console.log('Count comes back as this: ' + JSON.stringify(count));
				$scope.pagingFlags.postCount = count;
				//$log.log('TOTAL number of posts in : ' + $scope.pagingFlags.postCount);
			})
			.error(function (error) {
				$log.log('FAILURE loading blog post count: ' + error.message);
				$scope.status = 'Unable to load blog post count: ' + error.message;
			});
		postFactory.getPosts(pageToFetch, categoryToFetch)
			.success(function (posts) {
				//Make sure we get an array of at least one item.
				if (Object.prototype.toString.call(posts) === '[object Array]') {
					if (posts.length > 0) {
						$scope.posts = posts;
						//$log.log('TOTAL number of posts: ' + posts.length);
						calculatePaging($log);
					}
				}
			})
			.error(function (error) {
				$log.log('FAILURE loading blog posts: ' + error.message);
				$scope.status = 'Unable to load blog data: ' + error.message;
			});
	}

	function calculatePaging($log) {
		//calculate if there are older or newer pages...
		var totalPages = parseInt($scope.pagingFlags.postCount / settingsFactory.getPageSize()) + 1;
		$scope.pagingFlags.totalPages = totalPages;
		$scope.pagingFlags.currentPage = pageToFetch;
		if (1 >= pageToFetch) {
			$scope.pagingFlags.isNewer = false;
		} else if (1 < pageToFetch) {
			$scope.pagingFlags.isNewer = true;
		}
		if (totalPages > pageToFetch) {
			$scope.pagingFlags.isOlder = true;
		} else if (totalPages <= pageToFetch) {
			$scope.pagingFlags.isOlder = false;
		}
	}
}]);

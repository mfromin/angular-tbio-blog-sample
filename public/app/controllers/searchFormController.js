angular.module('blogApp').controller('SearchFormController', ['$scope', '$log', '$window', 'settingsFactory', function ($scope, $log, $window, settingsFactory) {
	//$log.log('Loading SearchPostsController');
	$scope.status; //currently not used - could use to state status on page.
	$scope.searchString = "";

	$scope.startSearch = function () {
		//$log.log('Start search with ... ' + $scope.searchString);
		//redirect to the URL for the search page...
		$window.location.href = '#/posts/search?search=' + $scope.searchString;

	};
}]);

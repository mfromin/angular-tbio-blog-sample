angular.module('blogApp').controller('SearchPostsController', ['$scope', '$routeParams', '$log', '$location', '$anchorScroll', 'postFactory', 'settingsFactory', function ($scope, $routeParams, $log, $location, $anchorScroll, postFactory, settingsFactory) {
	//$log.log('Loading SearchPostsController');
	var pageToFetch = 1;
	var categoryToFetch = null;
	var searchString = "";
	//	if ($location.search().page) {
	//		//$log.log('Page requested: ' + JSON.stringify($location.search().page));
	//		var tempPage = parseInt($location.search().page);
	//		if (typeof tempPage === 'number' && (tempPage % 1) === 0) {
	//			pageToFetch = tempPage;
	//		} else {
	//			$location.search('page', null);
	//		}
	//	}
	//
	//	if ($location.search().category) {
	//		//$log.log('Category requested: ' + JSON.stringify($location.search().category));
	//		categoryToFetch = $location.search().category;
	//	} else {
	//		$location.search('category', null);
	//	}

	if ($location.search().search) {
		//$log.log('Category requested: ' + JSON.stringify($location.search().category));
		searchString = $location.search().search;
	} else {
		$location.search('search', null);
	}


	$scope.status; //currently not used - could use to state status on page.
	$scope.posts;
	$scope.hideFlags = {
		pager: true,
		categories: true
	}
	$scope.pageTitles = {
		pageHeading: "Search Results for: ",
		secondaryText: searchString
	};
	$scope.pagingFlags = {
		isNewer: false,
		isOlder: true,
		totalPages: 1,
		currentPage: 1,
		postCount: 0
	};
	getPosts();

	$scope.changePage = function (pageIncrement) {
		pageToFetch = pageToFetch + pageIncrement;
		settingsFactory.setCurrentPage(pageToFetch);
		getPosts();
		$location.search('page', pageToFetch);
		$anchorScroll('top');
	};

	function getPosts() {
		postFactory.getPostsSearch(searchString)
			.success(function (posts) {
				//Make sure we get an array of at least one item.
				if (Object.prototype.toString.call(posts) === '[object Array]') {
					if (posts.length > 0) {
						$scope.posts = posts;
						$scope.pagingFlags.postCount = posts.length;
						//$log.log('TOTAL number of posts: ' + posts.length);
						calculatePaging($log);
					}
				}
			})
			.error(function (error) {
				$log.log('FAILURE loading search findings: ' + error.message);
				$scope.status = 'Unable to load search findings: ' + error.message;
			});
	}

	function calculatePaging($log) {
		//calculate if there are older or newer pages...
		var totalPages = parseInt($scope.pagingFlags.postCount / settingsFactory.getPageSize()) + 1;
		$scope.pagingFlags.totalPages = totalPages;
		$scope.pagingFlags.currentPage = pageToFetch;
		if (1 >= pageToFetch) {
			$scope.pagingFlags.isNewer = false;
		} else if (1 < pageToFetch) {
			$scope.pagingFlags.isNewer = true;
		}
		if (totalPages > pageToFetch) {
			$scope.pagingFlags.isOlder = true;
		} else if (totalPages <= pageToFetch) {
			$scope.pagingFlags.isOlder = false;
		}
	}
}]);

angular.module('blogApp').controller('SinglePostController', ['$scope', '$window', '$routeParams', '$log', 'postFactory', '$modal', function ($scope, $window, $routeParams, $log, postFactory, $modal) {

	//$log.log('ROUTE PARAMS: ' + $routeParams.postId);
	//Get one post here...
	$scope.status;
	$scope.post;
	$scope.tagArray = [];
	getPost();

	function getPost() {
		postFactory.getPost($routeParams.postId)
			.success(function (post) {
				$scope.post = post;
				if (post.tags) {
					$scope.tagArray = post.tags.split(',');
					for (var i = 0, len = $scope.tagArray.length; i < len; i++) {
						$scope.tagArray[i] = $scope.tagArray[i].replace(/"/g, '');
					}
				}
				//$log.log('$scope.tagArray:' + $scope.tagArray.length);
			})
			.error(function (error) {
				$log.log('FAILURE: ' + error.message);
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
	}

	$scope.confirm = function (size) {
		var modalInstance = $modal.open({
			templateUrl: 'app/views/confirmDeleteModal.html',
			controller: 'ModalInstanceCtrl',
			size: size,
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});

		modalInstance.result.then(function () {
			//$log.log('OK Clicked - we should delete post with id: ' + $routeParams.postId);
			//Do OK stuff
			postFactory.deletePost($routeParams.postId)
				.success(function (post) {
					//$log.log('SUCCESS...Post deleted!');
					$window.location.href = '#/';
				})
				.error(function (error) {
					$log.log('FAILURE: ' + error.message);
					$scope.status = 'Unable to load customer data: ' + error.message;
				});
		}, function () {
			//$log.log('CANCEL Clicked');
			//Do CANCEL stuff - for now there is nothing to do...
		});
	};

}]);

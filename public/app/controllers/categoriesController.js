angular.module('blogApp').controller('CategoriesController', ['$scope', '$log', '$location', 'settingsFactory', 'categoryFactory', function ($scope, $log, $location, settingsFactory, categoryFactory) {
	//$log.log('Loading CategoriesController');
	$scope.status; //currently not used - could use to state status on page.
	getCategories();

	function getCategories() {
		categoryFactory.getCategories()
			.success(function (categories) {
				$scope.categories = categories;
			})
			.error(function (error) {
				$log.log('FAILURE loading blog posts: ' + error.message);
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
	}

}]);

//Directive to highlight the current category in the category list on the home page.
//In this file as its specific to this page at runtime.
angular.module('blogApp').directive('detectActiveLink', ['$location', '$log', function ($location, $log) {
	return {
		link: function postLink(scope, element, attrs) {
			//cull the ?category=xxxxxxxx data from the current URL and each tab's URL so we can highlight the right category
			function getURLParameter(url, name) {
				return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url) || [, ""])[1].replace(/\+/g, '%20')) || null
			}
			scope.$watch("$location.path", function (newPath) {
				var categoryInPATH = getURLParameter($location.url(), 'category');
				//$log.log('categoryInPATH: ' + categoryInPATH);
				var categoryInURL = getURLParameter(attrs.href, 'category');
				//$log.log('categoryInURL: ' + categoryInURL);
				// now compare the two:
				if (categoryInPATH == categoryInURL) {
					element.parent().addClass("active");
					element.prev().addClass('glyphicon-star-empty');
					var span = $('<span />').html(element.html()).addClass("active");
					element.replaceWith(span);
				} else {
					element.removeClass("active");
					element.prev().removeClass('glyphicon-star-empty');
					element.parent().removeClass("active");
				}
			});
		}
	};
}]);

angular.module('blogApp').controller('CreatePostController', ['$scope', '$window', '$routeParams', '$sanitize', '$modal', '$log', 'postFactory', 'categoryFactory', 'settingsFactory', function ($scope, $window, $routeParams, $sanitize, $modal, $log, postFactory, categoryFactory, settingsFactory) {

	//if category selected return to the homepage with that category
	var returnURL = '#/';
	var currentCategory = settingsFactory.getCategoryId();
	if (currentCategory) {
		returnURL = '#/posts/category/' + currentCategory;
	}

	$scope.editor;
	$scope.post = {
		//item to be filled in with form data and date calculation.
	};
	$scope.categories;
	getCategories();

	function getCategories() {
		categoryFactory.getCategories()
			.success(function (categories) {
				$scope.categories = categories;
			})
			.error(function (error) {
				$log.log('FAILURE loading blog posts: ' + error.message);
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
	}

	$scope.staticText = {
		saveButtonText: 'Save New Post',
		cancelButtonText: 'Cancel New Post',
		pageTitle: "New Blog Post"
	};

	$scope.confirm = function (size) {
		var modalInstance = $modal.open({
			templateUrl: 'app/views/confirmCancelModal.html',
			controller: 'ModalInstanceCtrl',
			size: size,
			resolve: {
				items: function () {
					return $scope.items;
				}
			}
		});
		modalInstance.result.then(function () {
			//$log.log('OK Clicked - we should cancel the post creation/update');
			//Do OK stuff
			$window.location.href = returnURL;
		}, function () {
			//$log.log('CANCEL Clicked');
			//Do CANCEL stuff - for now there is nothing to do...
		});
	};

	if ($routeParams.postId) {
		//$log.log('$routeParams.postId: ' + $routeParams.postId);
		postFactory.getPost($routeParams.postId)
			.success(function (post) {
				$scope.post = post;
				if (post.tags) {
					post.tags = post.tags.replace(/,/g, " ");
				}
				$scope.staticText = {
					saveButtonText: 'Update Post',
					cancelButtonText: 'Cancel Update',
					pageTitle: "Edit Blog Post"
				};
			})
			.error(function (error) {
				$log.log('FAILURE: ' + error.message);
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
	}

	$scope.submitTheForm = function (isValid) {
		if ($scope.post) {
			//$log.log('SUBMIT should happen ? ' + isValid);
			//$log.log('$scope.post.title  ' + $scope.post.title);
			//$log.log('$scope.post.author  ' + $scope.post.author);
			//$log.log('$scope.post.tags  ' + $scope.post.tags);
			//$log.log('$scope.post.category  ' + $scope.post.category);
			//$log.log('$scope.post.imageURL  ' + $scope.post.imageURL);
			//$log.log('$scope.post.teaserContent  ' + $scope.post.teaserContent);
			//$log.log('$scope.post.fullContent  ' + $scope.post.fullContent);
		}
		//Save the content to the MongoDB...
		if (isValid) {
			if ($scope.post._id) {
				//$log.log('I already have an ID!!!');
				postFactory.updatePost($scope.post)
					.success(function (post) {
						//$log.log('SUCCESS updating post');
						$window.location.href = returnURL;
					})
					.error(function (error) {
						$log.log('FAILURE loading blog posts: ' + error.message);
						$scope.status = 'Unable to load customer data: ' + error.message;
					});
			} else {
				//$log.log('I must be NEW because there is no ID!!!');
				$scope.post.postDate = new Date();
				//$log.log('$scope.post.postDate  ' + $scope.post.postDate);
				postFactory.insertPost($scope.post)
					.success(function (post) {
						//$log.log('SUCCESS creating post');
						$window.location.href = returnURL;
					})
					.error(function (error) {
						$log.log('FAILURE loading blog posts: ' + error.message);
						$scope.status = 'Unable to load customer data: ' + error.message;
					});
			}
		} else {
			$log.log('Form not valid!');
		}
	};

}]);

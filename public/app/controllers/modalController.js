angular.module('blogApp').controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

	$scope.ok = function () {
		$modalInstance.close('ok');
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});

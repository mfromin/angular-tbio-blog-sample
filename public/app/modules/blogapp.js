angular.module('blogApp', ['ngRoute', 'ngSanitize', 'ui.bootstrap', 'ephox.textboxio']);

angular.module('blogApp').config(function ($routeProvider) {

	//routes for my application go here...
	$routeProvider
		.when('/posts/search', {
			controller: 'SearchPostsController',
			templateUrl: 'app/views/posts.html'
		})
		.when('/post/:postId', {
			controller: 'SinglePostController',
			templateUrl: 'app/views/post.html'
		})
		.when('/posts', {
			controller: 'BlogPostsController',
			templateUrl: 'app/views/posts.html'
		})
		.when('/about', {
			controller: 'EmptyController',
			templateUrl: 'app/views/about.html'
		})
		.when('/contact', {
			controller: 'EmptyController',
			templateUrl: 'app/views/contact.html'
		})
		.when('/createPost', {
			controller: 'CreatePostController',
			templateUrl: 'app/views/createPost.html'
		})
		.when('/editPost/:postId', {
			controller: 'CreatePostController',
			templateUrl: 'app/views/createPost.html'
		})
		.otherwise({
			redirectTo: '/posts'
		});
});

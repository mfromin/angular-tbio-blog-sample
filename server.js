// server.js

// BASE SETUP
// =================================================
var express = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');

// Connect to MongoDB - using mongoose
var mongoose = require('mongoose');
//mongoose.connect('mongodb://rest-test:XXXXXXXXXXX@ds061611.mongolab.com:61611/rest-test');
mongoose.connect('mongodb://localhost/rest-test');

//allow all CORS requests - makes local testing easier!
app.use(cors());

//configure app to use bodyParser() so we can get POST data
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

var port = process.env.PORT || 8080

// set the static files location e.g. /public/img will be /img for users
app.use(express.static(__dirname + '/public'));

// ROUTES FOR OUR API
// =================================================
require('./app/routes/routes.js')(express, app);


// START THE SERVER
// =================================================
app.listen(port);
console.log('Magic is now happening on port: ' + port);
